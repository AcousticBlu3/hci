var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('MyCtrl', function($scope, $window, $http) {
    var vm = this;

    vm.searchText = "";
    vm.filmovi = [];
    vm.sviFilmovi = [];
    vm.el = "";
    vm.email="";
    vm.password="";
    vm.message="";
    vm.autorizovan = false;


    vm.zanrovi = [
        { "id" : 12, "name" : "Adventure" },
        { "id" : 28, "name" : "Action" },
        { "id" : 14, "name" : "Fantasy" },
        { "id" : 878, "name" : "Science Fiction" },
        { "id" : 53,  "name" : "Thriller" },
        { "id" : 10751, "name" : "Family" },
        { "id" : 10749, "name" : "Romance"} 
    ];

    vm.filter = function(el){
        if(el == 'all'){
            vm.filmovi = vm.sviFilmovi;
        }else{
            var lista = [];
            for(var i in vm.sviFilmovi){
                var film = vm.sviFilmovi[i];
                var genres = eval(film.genres)
                for(var j in genres){
                    if(genres[j].name == el.name){
                        lista.push(film);
                        break;
                    }
                }
            }
            vm.filmovi = lista;
        }

    };
    vm.login = function(){
        if (vm.email =='srdjan.sretenovic970@gmail.com' && vm.password=='srdjans'){
            vm.message="Prijavljen je korisnik: "+vm.email;
            vm.autorizovan=true;

        }else{
            vm.message = "Pogresan unos podataka!";
            vm.autorizovan=false;
        }
    }
    vm.logout = function(){
        vm.email= "";
        vm.password = "";
        vm.autorizovan = false;
        vm.message = "Hvala sto ste koristili aplikaciju.";
      }
    vm.search = function(){
        vm.filmovi = [];
        for(var i in vm.sviFilmovi){
            var film = vm.sviFilmovi[i];
            if(film.title.toLowerCase().match(vm.searchText.toLowerCase())){
                vm.filmovi.push(film);
            }
        }
    };

    vm.get = function(){
      var req2 = {
          method: "GET",
          url: "/filmovi"
      }
      $http(req2).then(
          function(resp){
            console.log(resp);
            vm.filmovi = resp.data.filmovi;
            vm.sviFilmovi = resp.data.filmovi;
          }, function(resp){
              vm.message = 'error';
          });
    };

    vm.set_favorit = function(el){
      var data = {
        'imdb_id':el.imdb_id,
        'title': el.title
      }
      var req2 = {
          method: "POST",
          data: el,
          url: "/favorit"
      }
      $http(req2).then(
          function(resp){
            console.log(resp);
          }, function(resp){
              vm.message = 'error';
          });
    };


    vm.get();

});
