var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('MyCtrl', function($scope, $window, $http,  $uibModal) {
    var vm = this;

    vm.searchText = "";
    vm.filmovi = [];
    vm.sviFilmovi = [];
    vm.el = "";
    vm.omiljeniFilmovi=[];
    vm.losaForma=false;
    vm.username="";
    vm.password="";

    
    vm.editO = {
        'title':'',
        'popularity':0
    }


    

    vm.zanrovi = [
        { "id" : 12, "name" : "Adventure" },
        { "id" : 28, "name" : "Action" },
        { "id" : 14, "name" : "Fantasy" },
        { "id" : 878, "name" : "Science Fiction" },
        { "id" : 53,  "name" : "Thriller" },
        { "id" : 10751, "name" : "Family" },
        { "id" : 10749, "name" : "Romance"} 
    ];

    vm.filter = function(el){
        if(el == 'all'){
            vm.filmovi = vm.sviFilmovi;
        }else{
            var lista = [];
            for(var i in vm.sviFilmovi){
                var film = vm.sviFilmovi[i];
                var genres = eval(film.genres)
                for(var j in genres){
                    if(genres[j].name == el.name){
                        lista.push(film);
                        break;
                    }
                }
            }
            vm.filmovi = lista;
        }

    };
    vm.omiljeniF = function(){
        vm.filmovi=vm.omiljeniFilmovi;

    
       
    };

    vm.login = function () {
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'loginModal.html',
          controller: function($uibModalInstance, parent){
              var $ctrl = this;
  
              $ctrl.username = parent.username;
              $ctrl.password = parent.password;
              
              $ctrl.login = function(){
                $uibModalInstance.close($ctrl.username);
              }
              $ctrl.register = function(){
                if($ctrl.password.length<6){
           
                    
                }else if ($ctrl.password.length>=6){
                    
                $uibModalInstance.close($ctrl);
                }
              }
              /*$ctrl.change = function(){
                if($ctrl.password.length<6){
           
                    $ctrl.losaForma = true;
                    
                  }else{
                      $ctrl.losaForma = false;
                  }
              }*/
              $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
              $ctrl.switch = function(){
                $ctrl.createAcc=true;
            };
          },
          controllerAs: '$ctrl',
          resolve: {
            parent: function () {
              return vm;
            }
          }
        });
  
        modalInstance.result.then(function (username) {
          console.log(username);
          
        }, function (username) {
          console.log('modal-component dismissed at: ' + new Date() + 'with message:' + username);
        });
      };
      vm.edit = function (el) {
          vm.editO.title= el.title;
          vm.editO.popularity = el.popularity;
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'editModal.html',
          controller: function($uibModalInstance, parent){
              var $ctrl = this;
  
              $ctrl.title = parent.editO.title;
              $ctrl.popularity = parent.editO.popularity;
  
              $ctrl.edit = function(){
                
                $uibModalInstance.close($ctrl);
                
              }
  
  
              $ctrl.cancel = function () {
                $uibModalInstance.dismiss('cancel');
              };
          },
          controllerAs: '$ctrl',
          resolve: {
            parent: function () {
              return vm;
            }
          }
        }); 
        
  
        modalInstance.result.then(function (ctrl) {
          el.title = ctrl.title;
          el.popularity = ctrl.popularity;
          console.log(ctrl.popularity);
        }, function (title) {
          console.log('modal-component dismissed at: ' + new Date() + 'with message:' + title);
        });
      };
    
    vm.search = function(){
        vm.filmovi = [];
        for(var i in vm.sviFilmovi){
            var film = vm.sviFilmovi[i];
            if(film.title.toLowerCase().match(vm.searchText.toLowerCase())){
                vm.filmovi.push(film);
            }
        }
    };

    vm.get = function(){
      var req2 = {
          method: "GET",
          url: "/filmovi"
      }
      $http(req2).then(
          function(resp){
            console.log(resp);
            vm.filmovi = resp.data.filmovi;
            vm.sviFilmovi = resp.data.filmovi;
          }, function(resp){
              vm.message = 'error';
          });
    };

    vm.set_favorit = function(el){
      if(el.favorit){
          el.favorit=false;
          //var index = vm.omiljeniFilmovi.indexOf(el);
          vm.omiljeniFilmovi.splice(vm.omiljeniFilmovi.indexOf(el), 1);    
      }else{
          el.favorit=true;
          vm.omiljeniFilmovi.push(el);
      }
    };
    vm.brisanje = function(el){
        //var index = vm.filmovi.indexOf(el);
        vm.filmovi.splice(vm.filmovi.indexOf(el),1); 
       
    };
    

    vm.get();

});
