(function(angular){
    var app = angular.module("DrugaAplikacija",[]);
    
    app.controller("KorisnikCtrl", function($scope){
        $scope.ime="Srdjan";
        $scope.prezime="Sretenovic";
        $scope.proizvodi= [
            {naziv: "Pomorandza",kolicina:3, cena:120},
            {naziv: "Jabuka",kolicina:2, cena:20},
            {naziv: "Breskva",kolicina:4, cena:140},
        ];
        getTotal();
        
        function getTotal() {
            $scope.total=0;
            $scope.proizvodi.forEach(p => {
                $scope.total+= (p.cena*p.kolicina);
            });
        }
    });
        

})(angular);