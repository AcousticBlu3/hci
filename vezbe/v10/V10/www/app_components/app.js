var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('ProizvodiCtrl', function($http) {
    var vm = this;
    vm.object= {};
    vm.edit= {};
    vm.edit.id = '';
    vm.edit.naziv = '';
    vm.edit.cena = 0;
    vm.edit.kolicina = 0;
    vm.object.id = '';
    vm.object.naziv = '';
    vm.object.cena = 0;
    vm.object.kolicina = 0;
    
    
    vm.izmena=false;

    vm.lista = [];

    

    vm.init = function(){
        var req = {
            method:'GET',
            url:'/api/proizvodi'
            //data, ukoliko se salju neki podaci
        }
        $http(req).then(function(res){
            // pozvace se ako je ok
            vm.lista = res.data.proizvodi;
        } , function(res){
            // pozvace se ako je greska
        });
    }
    vm.add = function(el){
        var req ={
            method:'POST',
            url:'/api/proizvodi/add',
            data: el

        }
        $http(req).then(function(res){
            vm.init();
            vm.object = {
                cena:0,
                kolicina: 0
            }
            
        } , function(res){
            // pozvace se ako je greska
        });
        
      }
      vm.save = function(el){
        vm.izmena=true;
        var req ={
            method:'PUT',
            url:'/api/proizvodi/save',
            data: vm.edit

        }
        $http(req).then(function(res){
            vm.init();
            vm.edit = {
                id:"",
                naziv:"",
                cena:0,
                kolicina: 0
            }
            
        } , function(res){
            // pozvace se ako je greska
        });
        
      }
    vm.delete = function(el){
        var req= {
            method: 'DELETE',
            url:'/api/proizvodi/delete',
            data: el
        }
        $http(req).then(function(res){
            vm.init()
            
            
        } , function(res){
            // pozvace se ako je greska
        });
    }
    vm.forma = function(el){
        vm.izmena=true;
        vm.edit.id = el.id;
        vm.edit.naziv = el.naziv;
        vm.edit.cena = el.cena;
        vm.edit.kolicina= el.kolicina;
      }
    vm.potvrdi = function(){
        vm.save();
        vm.izmena=false;
      }
      vm.otkazi = function(){
        
        vm.izmena=false;
      }
    vm.init();
});

