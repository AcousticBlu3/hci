(function(angular){
    var app = angular.module("MojApp",[]);
    app.controller("MojCtrl", function($scope){
$scope.prva=0;
$scope.druga=0;
$scope.obim=0;
$scope.povrsina=0;
$scope.dijagonala=0;

$scope.izracunaj=function(){
    $scope.povrsina= ($scope.prva * $scope.druga);
    $scope.obim=(2*$scope.prva)+(2*$scope.druga);
    $scope.dijagonala= Math.sqrt(Math.pow($scope.prva,2)+Math.pow($scope.druga,2));
}
    });
})(angular);