var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('MyCtrl', function($scope, $window, $http) {
    var vm = this;

    vm.searchText = "";
    vm.filmovi = [];
    vm.pretraga = [];

    vm.search = function(){
        vm.pretraga=[];
        for(i=0;i<vm.filmovi.length;i++){

        }
    };

    vm.get = function(){
      var req2 = {
          method: "GET",
          url: "/filmovi"
      }
      $http(req2).then(
          function(resp){
            console.log(resp);
            vm.filmovi = resp.data.filmovi;
            vm.pretraga= resp.data.filmovi;
          }, function(resp){
              vm.message = 'error';
          });
    };

    vm.set_favorit = function(el){
      var data = {
        'imdb_id':el.imdb_id,
        'title': el.title
      }
      var req2 = {
          method: "POST",
          data: el,
          url: "/favorit"
      }
      $http(req2).then(
          function(resp){
            console.log(resp);
          }, function(resp){
              vm.message = 'error';
          });
    };


    vm.get();

});
